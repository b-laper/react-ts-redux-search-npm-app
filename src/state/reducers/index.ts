import { combineReducers } from 'redux'
import RepositoriesReducersState from './repositoriesReducer'

const reducers = combineReducers({
  repositories: RepositoriesReducersState,
})

export default reducers

export type RootState = ReturnType<typeof reducers>
